" Fuzen's Vim Configuration
"
"
" MIT License

" Copyright (c) 2019 Fuzen-py

" Permission is hereby granted, free of charge, to any person obtaining a copy
" of this software and associated documentation files (the \"Software\"), to deal
" in the Software without restriction, including without limitation the rights
" to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
" copies of the Software, and to permit persons to whom the Software is
" furnished to do so, subject to the following conditions:

" The above copyright notice and this permission notice shall be included in all
" copies or substantial portions of the Software.

" THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
" IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
" FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
" AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
" LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
" OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
" SOFTWARE.

" TODO: Add laptop mode (Battery Saving)
scriptencoding utf-8
if &compatible
	set nocompatible
endif
" Script Config - Core variables & setup for the rest of the config {{{1
let s:vim_plug_uri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
let s:vim_path = fnamemodify($MYVIMRC, ':p:h')
if has('unix')
	let s:uname = substitute(system("uname -s"), '\n','', 'g')
	let s:is_darwin = s:uname ==? "Darwin"
else
	echo "Unsupported"
	let s:is_darwin = 0
endif
if executable("yarn")
	let s:use_coc = 1
	let s:use_deoplete = 0
elseif has("python")
	let s:use_coc = 0
	let s:use_deoplete = 1
else
	let s:use_coc = 0
	let s:use_deoplete = 0
endif
" Disable Python2
let g:loaded_python_provider=1
" }}}
" Make Required Directories - autoload/swapfiles/undofiles/Ultisnips dirs {{{1
call mkdir(expand(s:vim_path . '/autoload'), 'p')
call mkdir(expand(s:vim_path . '/swapfiles'), 'p')
call mkdir(expand(s:vim_path . '/undofiles'), 'p')
call mkdir(expand(s:vim_path . '/UltiSnips'), 'p')
" }}}
" Ale - Ale lint configuraiton {{{1
" let g:ale_completion_enabled = !(s:use_coc || s:use_deoplete) " Enable ALE Completions if no other engine is used
let g:ale_fix_on_save = 1
let g:ale_rust_rls_config = { 'rust': { 'clippy_preference': 'on' } }
let g:ale_linters = { 'rust': ['rls', 'cargo'] }
let g:ale_rust_cargo_use_clippy = executable("cargo-clippy") " Enable clippy if exists
" }}}
" Install Plugin Manager - Install vim-plug on *unix systems {{{1
if has('unix')
	if empty(glob(expand(s:vim_path . '/autoload/plug.vim')))
		echo "Installing vim-plug"
		silent execute '!curl -fLo "' . expand(s:vim_path . '/autoload/plug.vim') . '" --create-dirs ' . s:vim_plug_uri
		autocmd VimEnter * PlugInstall | source $MYVIMRC
	endif
else
	if empty(glob(expand(s:vim_path . '/autload/plug.vim')))
		echo "Please install https://github.com/junegunn/vim-plug "
		exit
	endif
endif
" }}}
" Install Plugins {{{1
call plug#begin()
Plug 'morhetz/gruvbox' " Theme
Plug 'vim-airline/vim-airline' " Status line
Plug 'tpope/vim-sensible' " Sensible Default
Plug 'tpope/vim-repeat' " . the world
Plug 'tpope/vim-surround' " quoting/parenthesizing made simple
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } | Plug 'junegunn/fzf.vim' " Fuzzy finder
Plug 'tmux-plugins/vim-tmux-focus-events', !empty($TMUX) ? {} : { 'on': [] } " Tmux helper
Plug 'tmux-plugins/vim-tmux', !empty($TMUX) ? {} : { 'on': [] } " Tmux Helpee
Plug 'tpope/vim-markdown', { 'for': ['markdown'] } " Markdown stuff
Plug 'mattn/webapi-vim',  { 'for': ['rust'] } " Required for Rust-playground
Plug 'rust-lang/rust.vim', { 'for': ['rust'] } " Rust
Plug 'kopischke/vim-stay' " Save cursor position between sessions
Plug 'tpope/vim-fugitive' " Git
Plug 'jreybert/vimagit' " Git
Plug 'airblade/vim-gitgutter' " Git
Plug 'ap/vim-css-color', {'for': 'css'} " Show the color of color codes
Plug 'tpope/vim-unimpaired'
Plug 'wellle/targets.vim' " provides additional text objects
Plug 'Konfekt/FastFold' " Faster folding
Plug 'tpope/vim-commentary' " Comments
Plug 'w0rp/ale' " Aync Lint Engine / language server
Plug 'scrooloose/nerdtree', {'on': ['NERDTree']} | Plug 'Xuyuanp/nerdtree-git-plugin' " Nerd Tree
Plug 'Yggdroot/indentLine' " Indentation lines
Plug 'luochen1990/rainbow' " Rainbow paren
Plug 'mattn/emmet-vim', { 'for': ['html', 'css'] } " Emmet Support
Plug 'edkolev/tmuxline.vim', !empty($TMUX) ? {} : { 'on': [] } " Tmux Status Line
Plug 'ryanoasis/vim-devicons' " Dev Icons for file types
Plug 'sheerun/vim-polyglot' " Large collection of syntax highlighting
Plug 'editorconfig/editorconfig-vim' " Global editor configuration
Plug 'xu-cheng/brew.vim'
Plug 'majutsushi/tagbar'
Plug 'metakirby5/codi.vim', { 'on': ['Codi'] } " Python Interpriter

Plug 'https://gitlab.com/Fuzen-py/dark-mode.vim.git' " Dark Mode / Light Mode

if has('python')
	Plug 'SirVer/ultisnips' " Code Snippets
endif
Plug 'honza/vim-snippets' " More snippets
Plug 'chrisbra/csv.vim' " Comment seperated values
Plug 'rizzatti/dash.vim' " Documentation viewer (Zeal / Dash)
Plug 'lambdalisue/battery.vim' " Battery status
if s:is_darwin
	Plug 'srishanbhattarai/neovim-spotify', { 'do': 'bash install.sh' } " Spotify status
endif
if !s:use_coc
	Plug 'autozimu/LanguageClient-neovim', {
				\ 'branch': 'next',
				\ 'do': 'bash install.sh',
				\ } "Alternative Code Completion
endif
if s:use_coc
	Plug 'neoclide/coc.nvim', {'branch': 'release'} " Code completion
elseif s:use_deoplete
	Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' } " Alternative Code Completion
endif
call plug#end()
filetype plugin indent on
syntax enable
" }}}
" Dark Mode - Synchronize System & Vim {{{
let g:dark_mode#sync = 1
call dark_mode#set_detected_theme()
call dark_mode#watcher(10000)
" }}}
" General Settings {{{1
let g:mapleader = ","
let g:maplocalleader = ','
set termguicolors
set mouse=a
set modeline
set modelines=3
set noshowmode
set viewoptions=cursor,folds,slash,unix
syntax on
filetype plugin on
filetype plugin indent on
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
let $NVIM_TUI_ENABLE_CURSOR_SHAME=2
set hidden
set number
set backspace=indent,eol,start
set history=1000
set list
set smartcase
set report =0
execute 'set undodir='.expand(s:vim_path . '/undofiles')
set undofile
execute 'set dir='.expand(s:vim_path . '/swapfiles//')
set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
set autoindent
set clipboard=unnamedplus
" Smaller updatetime for CursorHold & CursorHoldI
set updatetime=300
" don't give |ins-completion-menu| messages.
set shortmess+=c
" always show signcolumns
set signcolumn=yes
autocmd filetype svn, *commit* setlocal spell " Spell checking
" Set cursor shape {{{2
if empty($TMUX)
	let &t_SI = "\<Esc>]50;CursorShape=1\x7"
	let &t_EI = "\<Esc>]50;CursorShape=0\x7"
	let &t_SR = "\<Esc>]50;CursorShape=2\x7"
else
	let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
	let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
	let &t_SR = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=2\x7\<Esc>\\"
endif " }}}
" Cursor follows window focus (x.x) {{{2
autocmd InsertLeave,WinEnter * set cursorline
autocmd InsertEnter,WinLeave * set nocursorline " }}}
" Relative Numbering {{{2
autocmd InsertLeave,BufRead * :let &relativenumber = 1
autocmd InsertEnter * :let &relativenumber = 0
if has('filterpipe')
	set noshelltemp
endif " }}}
" InvisChars {{{
if has('multi_byte') && &encoding ==# 'utf-8'
	set listchars=tab:▸\ ,extends:❯,precedes:❮,nbsp:±,trail:-,eol:¬
else
	set listchars=tab:>>\ ,eol:;,trail:-,extends:>,precedes:<,nbsp:+
endif " }}}
" Folding {{{2
set foldenable
set foldlevel=2

set foldmethod=syntax
"}}}
" }}}
" Gruvbox - configure gruvbox theme {{{1
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_contrast_light=g:gruvbox_contrast_dark
let g:gruvbox_italic=1
let g:gruvbox_bold=1
let g:gruvbox_underline=1
silent! colorscheme gruvbox " }}}
" Airline - Powerline & Battery status {{{1
let g:airline_symbols={}
let g:airline_detect_modified=1
let g:airline_detect_paste=1
let g:airline_detect_spell=1
let g:airline_detect_spelllang=1
let g:airline_detect_crypt=1
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = '㏑'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.whitespace = 'Ξ'
" Battery Status {{{
function! DrawBattery()
	if battery#value() == -1
		return ''
	end
	if battery#value() == 100 && battery#is_charging()
		return '•100%'
	endif
	return battery#sign() . battery#value() . '%'
endfunction
function! AddBattery(...)
	let builder = a:1
	let context = a:2
	if exists('*battery#is_charging') && exists('*battery#value') && battery#value() != -1
		call builder.add_section('airline_b', '%{DrawBattery()}')
	endif
	return 0
endfunction
silent! call airline#add_statusline_func('AddBattery')
let g:battery#update_statusline = 1
let g:battery#symbol_charging = '⇡'
let g:battery#symbol_discharging = '⇣'
" }}}
" }}}
" FastFold {{{1
let g:fastfold_fold_command_suffixes = ['x','X','a','A','o','O','c','C','r','R','m','M','i','n','N']
" }}}
" NerdTree {{{1
" }}}
" Rainbow parentheses {{{
let g:rainbow_active = 1
" let g:rainbow_conf = {
" 			\    'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick'],
" 			\    'ctermfgs': ['lightblue', 'lightyellow', 'lightcyan', 'lightmagenta'],
" 			\    'operators': '_,_',
" 			\    'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold'],
" 			\    'separately': {
" 			\        '*': {},
" 			\        'tex': {
" 			\            'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/'],
" 			\        },
" 			\        'lisp': {
" 			\            'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick', 'darkorchid3'],
" 			\        },
" 			\        'vim': {
" 			\            'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/', 'start=/{/ end=/}/ fold', 'start=/(/ end=/)/ containedin=vimFuncBody', 'start=/\[/ end=/\]/ containedin=vimFuncBody', 'start=/{/ end=/}/ fold containedin=vimFuncBody'],
" 			\        },
" 			\        'html': {
" 			\            'parentheses': ['start=/\v\<((area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr)[ >])@!\z([-_:a-zA-Z0-9]+)(\s+[-_:a-zA-Z0-9]+(\=("[^"]*"|'."'".'[^'."'".']*'."'".'|[^ '."'".'"><=`]*))?)*\>/ end=#</\z1># fold'],
" 			\        },
" 			\        'css': 0,
" 			\    }
" 			\}
" }}}
" Editor Config {{{1
let g:EditorConfig_exclude_patterns = ['fugitive://.*'] " }}}
" Deoplete {{{1
let g:deoplete#enable_at_startup = 1 " }}}
" Conquer of Completion {{{
let g:coc_global_extensions = [
			\ "coc-json",
			\ "coc-css",
			\ "coc-rls",
			\ "coc-highlight",
			\ "coc-emmet",
			\ "coc-snippets",
			\ "coc-html",
			\ "coc-python",
			\ "coc-yaml",
			\ "coc-phpls",
			\ "coc-tsserver",
			\ "coc-pairs",
			\ "coc-dictionary",
			\ "coc-tag",
			\ "coc-word",
			\ "coc-emoji",
			\ "coc-syntax"
			\ ]
"}}}
" Language Client {{{
if has("unix")
	let g:LanguageClient_serverCommands = {'rust': '/usr/bin/env rustup run rls', 'haskell': ['hei-wrapper']}
else
	let g:LanguageClient_serverCommands = {'rust': '~/.cargo/bin/rustup run rls', 'haskell': ['hei-wrapper']}
endif
" }}}
" Custom Commands {{{
" TrimWhiteSpace - Trim trailing whitespaces {{{2
function! s:TrimWhiteSpace() abort
	let l:save_cursor = getpos('.')
	silent! %s/\s\+$//e
	call setpos('.', l:save_cursor)
endfunction
command! TrimWhiteSpace call s:TrimWhiteSpace()
autocmd BufWritePre * call s:TrimWhiteSpace() " }}}
" TrimEndLines - Trim trailing end lines {{{2
function! s:TrimEndLines() abort
	let l:save_cursor = getpos('.')
	silent! %s#\($\n\s*\)\+\%$##
	" silent! %s/\s\+$//e
	call setpos('.', l:save_cursor)
endfunction
command! TrimEndLines call s:TrimEndLines()
autocmd BufWritePre * call s:TrimEndLines()
" }}}
" STab  - set tabstop, softtabstop and shiftwidth to the same value {{{2
function! SummarizeTabs()
	try
		echohl ModeMsg
		echon 'tabstop='.&l:ts
		echon ' shiftwidth='.&l:sw
		echon ' softtabstop='.&l:sts
		if &l:et
			echon ' expandtab'
		else
			echon ' noexpandtab'
		endif
	finally
		echohl None
	endtry
endfunction
function! Stab()
	let l:tabstop = 1 * input('set tabstop = softtabstop = shiftwidth = ')
	if l:tabstop > 0
		let &l:sts = l:tabstop
		let &l:ts = l:tabstop
		let &l:sw = l:tabstop
	endif
	call SummarizeTabs()
endfunction
command! -nargs=* STab call Stab() "}}}
" Gui Font Size - Bigger / Smaller, self explinatory {{{2
command! Bigger  :let &guifont = substitute(&guifont, '\d\+$', '\=submatch(0)+1', '')
command! Smaller :let &guifont = substitute(&guifont, '\d\+$', '\=submatch(0)-1', '')
" }}}
" NumberToggle - Switches between relativenumber and non-relative {{{2
function! s:NumberToggle()
	if(&relativenumber == 1)
		let &relativenumber = 0
	else
		let &relativenumber = 1
	endif
endfunction
command! NumberToggle call s:NumberToggle() " }}}
" Search - Find a file that contains <search> using ripgrep {{{2
command! -bang -nargs=* Search call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0) " }}}
" WordWrap - wrap lines by word {{{2
function! s:WordWrapToggle()
	let &lbr = (!&lbr)
endfunction
command! WordWrapToggle call s:WordWrapToggle() " }}}
" w!! - sudo write {{{
cmap w!! w !sudo tee > /dev/null %
"}}}
" }}}
" Keybinds {{{1
" Easy ale error navigation {{{2
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
" }}}
" Allow repeat indentation easily {{{2
xnoremap <  <gv
xnoremap >  >gv
" }}}
" CrtrlP {{{
nmap <C-p> :Search <enter>
" }}}
" LanguageClient {{{
if !s:use_coc
	nnoremap <F5> :call LanguageClient_contextMenu()<CR>
	map <Leader>lk :call LanguageClient#textDocument_hover()<CR>
	map <Leader>lg :call LanguageClient#textDocument_definition()<CR>
	map <Leader>lr :call LanguageClient#textDocument_rename()<CR>
	map <Leader>lf :call LanguageClient#textDocument_formatting()<CR>
	map <Leader>lb :call LanguageClient#textDocument_references()<CR>
	map <Leader>la :call LanguageClient#textDocument_codeAction()<CR>
	map <Leader>ls :call LanguageClient#textDocument_documentSymbol()<CR>
endif
" }}}
" }}}
" vim: tabstop=4:shiftwidth=4:softtabstop=4:noexpandtab:foldmethod=marker:
