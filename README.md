# Fuzen's NeoVim Config

## Features
---
- System Intergration
  - MacOS
    - Synchronize vim background with system dark mode
- Battery Detection (Only shows battery life)
- Completion
  - CoC (Requires Yarn)
  - Deoplete (Requires python-neovim)
  - LanguageClient-neovim (Fallback)
- AutoInstall PluginManager (Linux/MacOS)
- Gruvbox theme
- Indent markers
- Smart Relative numbering
- Git Intergration
- Show Invis Characters
- Cursor follows window focus
- Folding (syntax)
- Rainbow parentheses
- EditorConfig support
- AutoTrim  trailing white space & end lines
- Mouse Support

### Custom Commands:
- STab - Set Tabstop, softstop and shift width to be eq
- WordWrapToggle - Toggle Word Wrap
- TrimWhiteSpace - Trim trailing whitespace
- TrimEndLines - Trim trailing lines
- Search - search for text in files
- Bigger - set gui font to a larger size
- Smaller - set gui font to a smaller size
